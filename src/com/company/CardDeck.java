package com.company;

import java.util.Random;

/**
 * Created by user on 24.07.2017.
 */
public class CardDeck<T> {
    private T[] array;//nulls
    public static final Random RANDOM = new Random();
    public static final int WIN_NUMBER = 21;


    public CardDeck(T[] ints) {
        array = ints;
    }


    private T getKarta() {
        return array[RANDOM.nextInt(array.length)];
    }

    public void startGame() {

        int sumPoints = 0;

        while (true) {
            Karta karta = (Karta) getKarta();

            int point = karta.getNominal().getPoint();
            sumPoints += point;

            System.out.println("Karta: " + "\t\tSiut: " + karta.getSuit().name() + "\t\tNominal: " +
                    karta.getNominal().name() + "\t\tPoint: " + point);

            if (sumPoints < WIN_NUMBER) {
                continue;
            } else if (sumPoints == WIN_NUMBER) {
                System.out.println();
                System.out.println("Total points = " + sumPoints + "\t\t YOU ARE WIN !!!");
                break;
            } else {
                System.out.println();
                System.out.println("Total points = " + sumPoints + "\t\t YOU ARE FAIL !!!");
                break;
            }

        }
    }

}




