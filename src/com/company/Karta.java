package com.company;

/**
 * Created by user on 24.07.2017.
 */
public class Karta {
    private Nominal nominal;
    private Suit suit;

    Karta(Nominal newNominal, Suit newSuit) {
        nominal = newNominal;
        suit = newSuit;
    }


    public Nominal getNominal() {
        return nominal;
    }

    public Suit getSuit() {
        return suit;
    }

}
