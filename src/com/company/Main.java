package com.company;

public class Main {

    public static void main(String[] args) {

        Karta tuzBubni  =  new Karta(Nominal.TUZ, Suit.BUBNI);
        Karta tuzChirwa =  new Karta(Nominal.TUZ, Suit.CHIRWA);
        Karta tuzPiki   =  new Karta(Nominal.TUZ, Suit.PIKI);
        Karta tuzTrefi =  new Karta(Nominal.TUZ, Suit.TREFI);
        Karta twoBubni =  new Karta(Nominal.TWO, Suit.BUBNI);
        Karta twoChirwa = new Karta(Nominal.TWO, Suit.CHIRWA);
        Karta twoPiki =  new Karta(Nominal.TWO, Suit.PIKI);
        Karta twoTrefi = new Karta(Nominal.TWO, Suit.TREFI);
        Karta threeBubni = new Karta(Nominal.THREE, Suit.BUBNI);
        Karta threeChirwa = new Karta(Nominal.THREE, Suit.CHIRWA);
        Karta threePiki = new Karta(Nominal.THREE, Suit.PIKI);
        Karta threeTrefi = new Karta(Nominal.THREE, Suit.TREFI);
        Karta fourBubni = new Karta(Nominal.FOUR, Suit.BUBNI);
        Karta fourChirwa = new Karta(Nominal.FOUR, Suit.CHIRWA);
        Karta fourPiki = new Karta(Nominal.FOUR, Suit.PIKI);
        Karta fourTrefi = new Karta(Nominal.FOUR, Suit.TREFI);
        Karta fiveBubni = new Karta(Nominal.FIVE, Suit.BUBNI);
        Karta fiveChirwa = new Karta(Nominal.FIVE, Suit.CHIRWA);
        Karta fivePiki = new Karta(Nominal.FIVE, Suit.PIKI);
        Karta fiveTrefi = new Karta(Nominal.FIVE, Suit.TREFI);
        Karta sixBubni = new Karta(Nominal.SIX, Suit.BUBNI);
        Karta sixChirwa = new Karta(Nominal.SIX, Suit.CHIRWA);
        Karta sixPiki = new Karta(Nominal.SIX, Suit.PIKI);
        Karta sixTrefi = new Karta(Nominal.SIX, Suit.TREFI);
        Karta sevenBubni = new Karta(Nominal.SEVEN, Suit.BUBNI);
        Karta sevenChirwa = new Karta(Nominal.SEVEN, Suit.CHIRWA);
        Karta sevenPiki = new Karta(Nominal.SEVEN, Suit.PIKI);
        Karta sevenTrefi = new Karta(Nominal.SEVEN, Suit.TREFI);
        Karta eightBubni = new Karta(Nominal.EIGHT, Suit.BUBNI);
        Karta eightChirwa = new Karta(Nominal.EIGHT, Suit.CHIRWA);
        Karta eightPiki = new Karta(Nominal.EIGHT, Suit.PIKI);
        Karta eightTrefi = new Karta(Nominal.EIGHT, Suit.TREFI);
        Karta nineBubni = new Karta(Nominal.NINE, Suit.BUBNI);
        Karta nineChirwa = new Karta(Nominal.NINE, Suit.CHIRWA);
        Karta ninePiki = new Karta(Nominal.NINE, Suit.PIKI);
        Karta nineTrefi = new Karta(Nominal.NINE, Suit.TREFI);
        Karta tenBubni = new Karta(Nominal.TEN, Suit.BUBNI);
        Karta tenChirwa = new Karta(Nominal.TEN, Suit.CHIRWA);
        Karta tenPiki = new Karta(Nominal.TEN, Suit.PIKI);
        Karta tenTrefi = new Karta(Nominal.TEN, Suit.TREFI);
        Karta valetBubni = new Karta(Nominal.VALET, Suit.BUBNI);
        Karta valetChirwa = new Karta(Nominal.VALET, Suit.CHIRWA);
        Karta valetPiki = new Karta(Nominal.VALET, Suit.PIKI);
        Karta valetTrefi = new Karta(Nominal.VALET, Suit.TREFI);
        Karta damaBubni = new Karta(Nominal.DAMA, Suit.BUBNI);
        Karta damaChirwa = new Karta(Nominal.DAMA, Suit.CHIRWA);
        Karta damaPiki = new Karta(Nominal.DAMA, Suit.PIKI);
        Karta damaTrefi = new Karta(Nominal.DAMA, Suit.TREFI);
        Karta korolBubni = new Karta(Nominal.KOROL, Suit.BUBNI);
        Karta korolChirwa = new Karta(Nominal.KOROL, Suit.CHIRWA);
        Karta korolPiki = new Karta(Nominal.KOROL, Suit.PIKI);
        Karta korolTrefi = new Karta(Nominal.KOROL, Suit.TREFI);


        Karta[] cards = new Karta[]{
                tuzBubni, tuzPiki, tuzChirwa, tuzTrefi, twoBubni, twoPiki, twoChirwa, twoTrefi,
                threeBubni, threePiki, threeChirwa, threeTrefi, fourBubni, fourPiki, fourChirwa, fourTrefi,
                fiveBubni, fivePiki, fiveChirwa, fiveTrefi, sixBubni, sixPiki, sixChirwa, sixTrefi,
                sevenBubni, sevenPiki, sevenChirwa, sevenTrefi, eightBubni, eightPiki, eightChirwa, eightTrefi,
                nineBubni, ninePiki, nineChirwa, nineTrefi, tenBubni, tenPiki, tenChirwa, tenTrefi,
                valetBubni, valetPiki, valetChirwa, valetTrefi, damaBubni, damaPiki, damaChirwa, damaTrefi,
                korolBubni, korolPiki, korolChirwa, korolTrefi
        };

        CardDeck<Karta> cardDeck = new CardDeck(cards);

        cardDeck.startGame();

    }
}
