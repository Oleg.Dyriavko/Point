package com.company;

/**
 * Created by user on 24.07.2017.
 */
public enum Nominal {
    TUZ(1),
    TWO(2),
    THREE(3),
    FOUR(4),
    FIVE(5),
    SIX(6),
    SEVEN(7),
    EIGHT(8),
    NINE(9),
    TEN(10),
    VALET(11),
    DAMA(12),
    KOROL(13);

    private int point;

    Nominal(int newNominal) {
        point = newNominal;
    }

    public int getPoint() {
        return point;
    }
}
